" My vimrc is based on the the vimrc of Amix:
"   http://amix.dk/vim/vimrc.html
"
" Author: weralwolf
" Homepage:
" Email: weralwolf@gmail.com
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set nocompatible               "disable vi compatibility
filetype off                   "required!

" Moving around splits
if $VIM_CRONTAB == "true"
  set nobackup
  set nowritebackup
endif
inoremap <S-Left> <C-w><C-h>
inoremap <S-Right> <C-w><C-l>
inoremap <S-Up> <C-w><C-k>
inoremap <S-Down> <C-w><C-j>


nnoremap <S-Left> <C-w><C-h>
nnoremap <S-Right> <C-w><C-l>
nnoremap <S-Up> <C-w><C-k>
nnoremap <S-Down> <C-w><C-j>

" Moving around tabs
nnoremap <S-w> gT
nnoremap <S-e> gt

" Right marging. Nowaday screens are big, so 160 should be good solution
set colorcolumn=160
set backspace=indent,eol,start

syntax on
set foldmethod=manual
set foldnestmax=10
set nofoldenable
set foldlevel=4
set splitright

source ~/.vim/vim_modules.vim
source ~/.vim/powerline.vim
source ~/.vim/nerdtree.vim
source ~/.vim/airline.vim

"Enable filetype plugins
filetype plugin on          " required
filetype indent plugin on     " Enable loading the plugin files for specific file types

" Vim tabber config for Ubuntu
set tabline=%!tabber#TabLine()

" Create if not exists and open for editing
map <leader>gf :e <cfile><cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Set how many lines of history Vim has to remember
set history=10000

"Display the line number
set relativenumber
set number

set autoindent
set smartindent
set autoread       " Reload files when changed

set ruler
set undofile
" set relativenumber
set expandtab
set tabstop=2      " Set number of spaces that a <Tab> in the file counts for
set cindent
set shiftwidth=2   " Set width of nested tabs,control  how many columns text is indented with the reindent operations(<<)
set softtabstop=2

" autocmd: specify commands to be executed automatically when reading or
" writing a file, when entering or leaving a buffer or window
" More information, please refer to:
" http://learnvimscriptthehardway.stevelosh.com/chapters/12.html
autocmd BufRead,BufNewFile *.py set shiftwidth=4
autocmd BufRead,BufNewFile *.py set tabstop=4
autocmd BufRead,BufNewFile *.py set softtabstop=4

autocmd BufRead,BufNewFile *.js set shiftwidth=2
autocmd BufRead,BufNewFile *.js set tabstop=2
autocmd BufRead,BufNewFile *.js set softtabstop=2

autocmd BufRead,BufNewFile *.es6 set syntax=javascript
autocmd BufRead,BufNewFile *.js.map set syntax=json
autocmd BufRead,BufNewFile *.ts set syntax=typescript

" When setting showcmd, the bottom line will show you information about the
" current command going on
set showcmd

" Set vim default display encode
set encoding=utf-8
"set file encoding when save or new file
set fileencoding=utf-8
"file encode list,when vim read file,it will detect according to this config
set fileencodings=utf-8,gbk,gb2312
" set guifont=*
set guifont=Source\ Code\ Pro\ for\ Powerline "make sure to escape the spaces in the name properly

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vim UI
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Highlight the current line
set cursorline

"Highlight the search result and incremental search
set hlsearch
set incsearch

" Status line config
set laststatus=2  "Always show the status line
set t_Co=256

"Set the colortheme of vim
set termguicolors
let ayucolor="mirage" " for mirage version of theme
colorscheme ayu

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vim plugin config
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Latex preview
let g:livepreview_previewer = 'open -a Preview'

" UltiSnips
autocmd BufNewFile,BufRead *.html.erb set filetype=html.eruby

" ctrlP
let g:ctrlp_extensions = ['tag']

let g:ctrlp_working_path_mode='ra' " c: the directory of the current file; r: the nearest ancestor that contains one of these directories or files:.git .hg .svn .bzr

" Exclude files and directorys
" unlet g:ctrlp_custom_ignore
let g:ctrlp_custom_ignore = { 'dir': '\v[\/](\.git|\.hg|\.svn)$', 'file': '\v\.(so|swap|tar|zip|jar|jpg|png|pdf|doc|docx|gz|bz2|rpm|deb)$',}


" TagList
let Tlist_Inc_Winwidth=0
let Tlist_Process_File_Always=1
let Tlist_File_Fold_Auto_Close=1
set autochdir

" TagBar
set tags=./tags,tags;/
let g:tagbar_width=20   " Set tagbar window width

" YouComepleteMe
" Set YouCompleteMe trigger key
let g:ycm_key_list_select_completion = ['<Down>']
let g:ycm_key_list_previous_completion = ['<Up>']

" Syntastic plugin
let g:Syntastic_c_check_header = 1
let syntastic_mode_map = { 'passive_filetypes': ['html'] } " don't check html
let g:syntastic_c_checkers = ['gcc']
let g:syntastic_cpp_checkers = ['cppcheck' , 'gcc' , 'make']
let g:syntastic_matlab_checkers = ['mlint']
let g:syntastic_markdown_checkers = ['mdl']
let g:syntastic_text_checkers = ['language_check' , 'atdtool']

let g:syntastic_cpp_compiler = 'clang++' " C++ compiler
let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++' " C++11
let g:syntastic_cpp_compiler_options = ' -std=c++1y' " C++14 support

let g:syntastic_python_checkers = ['pycodestyle']
let g:syntastic_python_pycidestyle_args = '----max-line-length=160'

" Sparkup
let g:sparkupExecuteMapping = '<c-x>'
let g:sparkupNextMapping = '<c-e>'

" Vim-markdown
let g:vim_markdown_folding_disabled=1
autocmd BufNewFile,BufReadPost *.md set filetype=markdown    " *.md support


" Set shorcut key
nmap <F3> :TagbarToggle<CR>
" Stop automatic indentation when copied from another application at insert
set pastetoggle=<F2>

" search moving operations
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch

" hadle long lines correctly
set wrap

" Gundo mapping conf
nnoremap <F5> :GundoToggle<CR>

" 6to5
autocmd BufRead,BufNewFile *.es6 setfiletype javascript

" Collectiong vim-toxic output in one place
autocmd BufRead,BufNewFile *.es6 setfiletype javascript 207 set
set backupdir=~/.vim/toxic/backupdir//
set directory=~/.vim/toxic/directory//
set undodir=~/.vim/toxic/undodir//

" Remove trailing white spaces on save
autocmd BufWritePre * :%s/\s\+$//e
autocmd BufWritePre * :retab

if has('vim_starting')
  set nocompatible
  set runtimepath+=~/.vim/bundle/dart-vim-plugin
endif
filetype plugin indent on


" Define custom commands
com FindTODO :vimgrep /\<TODO\>/j **/*.py | :cope
com FindFIXME :vimgrep /\<FIXME\>/j **/*.py | :cope
