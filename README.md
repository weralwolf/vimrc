Vim configuration
===

Introduction
---

This my vim cofiguration which once starts from [DotVim](https://github.com/xautjzd/dotvim) and then starts to evolve within my needs.

How to use?
---

### 1. Clone my repository to your home directory.

```
$git clone https://weralwolf@bitbucket.org/weralwolf/vimrc.git ~/.vim
```

### 2. Unleash script

```
$unleash.sh
```

#### It would:

1. Create a soft link of `~/.vim/vimrc -> ~/.vimrc`
2. Store your previous `.vimrc` under strange name at `~/.vim/` path
3. Install vundle into `~/.vim/bundle/vundle/` path
4. Run installation of other plugins I usually use
5. Configure YouCompleteMe plugin and run its installation

#### It would not:

1. Install cmake for compilation of YouCompleteMe

2. Configure your OSx properly for last cmake, but next command might hel you:
```
$sudo brew reinstall xz --build-from-source
```

3. Install Powerline fonts for beautiful triangle corners at your status-line, but next commands might help you:

    `$cd ~/.vim/bundle/fonts/`

    `$sudo pip install --user powerline-status`

    `$./install.sh`

4. Read through [Powerline fonts installation](https://powerline.readthedocs.org/en/latest/installation.html#fonts-installation) instead of you

5. Install MacVim for you if you're happy OSx user, but... Next command might help you:
```
 brew install macvim --env-std --override-system-vim
```
6. Help you configure your iTerm2, but guy in a one blog wrote [Setup Vim, Powerline and iTerm2 on Mac OS X](https://coderwall.com/p/yiot4q/setup-vim-powerline-and-iterm2-on-mac-os-x)

`unleash.sh` would not do these 5 steps instead of you, because they are system specific, user specific and I'm lazy enough to let you do that if you
want and need.

### 4. Requirements

Install  development tools and CMake:

    Ubuntu: $sudo apt-get install build-essential cmake
    Fedora: $sudo yum install build-essential cmake
    OSx:    $brew install cmake # also take a look to 2."It would not".2

Make sure you have Python installed:

    Ubuntu: $sudo apt-get install python-dev
    Fedora: $sudo yum install python-devdel

About more details,please refer to [YouCompleteMe](https://github.com/Valloric/YouCompleteMe).

### 5. Install patched fonts

For the nice looking powerline symbols to appear, you will need to install a patched font. Instructions can be found in the official powerline [documentation][1]. Prepatched fonts can be found in the [powerline-fonts][2] repository

### 6. Further reading

1. [Coming home to Vim](http://stevelosh.com/blog/2010/09/coming-home-to-vim/)
2. [Don’t tell people to use VIM (because) You’re Using It Wrong 10 minutes to read](http://antjanus.com/blog/thoughts-and-opinions/use-vim/)
3. [Vim reference card](http://michaelgoerz.net/refcards/vimqrc.pdf)

[1]: https://powerline.readthedocs.org/en/latest/fontpatching.html
[2]: https://github.com/Lokaltog/powerline-fonts
