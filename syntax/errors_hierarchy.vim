" Vim syntax file
" Language: Errors Hierarchy Description
" Maintainer: Anatolii Koval
" Lastest Revision: 27 March 2016

if exists("b:current_syntax")
  finish
endif

syn match ehComment '\[[^\]]*\]'
syn match ehLiteral '\h*' nextgroup=ehComment skipwhite
syn match ehStructure '.--' nextgroup=ehLiteral skipwhite
syn match ehStructure '|' nextgroup=ehStructure skipwhite
syn match ehCommentLine '-.*$'
syn match ehConst '\" \h' nextgroup=ehCommentLine skipwhite

let b:current_syntax = "error_hierarchy"

hi def link ehStructure   Statement
hi def link ehClass       Statement
hi def link ehLiteral     PreProc
hi def link ehConst       Constant
hi def link ehComment     Comment
hi def link ehCommentLine Comment
