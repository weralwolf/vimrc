#!/bin/bash

echo "Cloning vundle plugin manager"
cd ~/.vim/

git clone https://github.com/gmarik/vundle.git bundle/vundle

echo "Creating soft link for loval vim config file"
[[ -a ~/.vimrc ]] && mv ~/.vimrc ~/.vim/vimrc.$(cat /dev/urandom| strings| head -c 300 | md5sum | head -c 32)
ln -s ./vimrc ~/.vimrc

# brew install macvim --env-std --override-system-vim
echo "Instaling vim pluginc with vundle"
echo | vim +PluginInstall +qall -u ~/.vim/vim_modules.vs

echo "Configuring plugins"
cd bundle/

## Install YouCompleteMe
cd YouCompleteMe/

echo "Perform YouCompleteMe plugin installation"
# sudo brew reinstall xz --build-from-source
./install.sh ~/.vimrc

# Markdown-instant
sudo npm -g install instant-markdown-d

## Install powerline/fonts
# cd fonts/
# pip install --user powerline-status
# ./install.sh
echo "Done!"

# Mac OSX problem with python:
# http://www.oschrenk.com/vim-youcompleteme-and-python/
