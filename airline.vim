" Vim-airline
" More airline-customization please refer to vim-airline doc:
" https://github.com/bling/vim-airline/blob/master/doc/airline.txt
let g:airline_powerline_fonts=1 " Let airline plugin use the arrow effect of powerline
let g:airline#extensions#whitespace#enabled = 0
let g:airline#extensions#tabline#enabled = 1
"set vim statusbar theme
let g:airline_theme="wombat"
let g:Powerline_symbols = 'fancy'
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
  endif

" To include/ungear integration with inderect plug-ins
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#bufferline#enabled = 1
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#csv#enabled = 1
let g:airline#extensions#csv#column_display = 'Name'
